# COzvena
COzvena is a secure hashing password manager using the scrypt algorithm. It is inspired by a paper by J. Alex Halderman, Brent Waters and Edward W. Felten: "A Convenient Method for Securely Managing Passwords".

COzvena is a C port of [Ozvena](https://gitlab.com/markonius/ozvena)

If you are interested in understanding the algorithm, here is a presentation from the author:  
[https://www.tarsnap.com/scrypt/scrypt-slides.pdf](https://www.tarsnap.com/scrypt/scrypt-slides.pdf)

If you are interested in why this app works the way it does, here is the above mentioned paper:  
[http://www.cs.utexas.edu/~bwaters/publications/papers/www2005.pdf](http://www.cs.utexas.edu/~bwaters/publications/papers/www2005.pdf)

### Special thanks

[Marko Žitković](https://gitlab.com/puzomorcro) - For teaching me enough C to stay sane.
