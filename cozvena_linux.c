void getPass(uint8_t* buffer, size_t size)
{
	for (int i = 0; i < size; i++)
	{
		char c = getchar();

		// Bail (ctrl+c, ctrl+d)
		if (c == 3 || c == 4)
			bail("User interupt\n");

		// Backspace
		if (c == 8)
		{
			i -= 2;
			if (i < -1)
				i = -1;
			continue;
		}

		// Accept
		if (c == '\n' || c == '\r')
		{
			buffer[i] = '\0';
			printf("\n");
			return;
		}

		// Cancel (ESC)
		if (c == 27) 
		{
			i = -1;
			continue;
		}

		if (c >= 32 && c <= 126)
		{
			buffer[i] = c;
		}
		else // ignore
		{
			i--;
		}
	}
	printf("\n");
	return;
}

void copyToClipboard(uint8_t* text)
{
	printf("%s\n", text);
}

