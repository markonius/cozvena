#include <conio.h>
#include <windows.h>

void getPass(uint8_t* buffer, size_t size)
{
	for (int i = 0; i < size; i++)
	{
		char c = getch();

		// Bug in cl
		if (c == '\0')
		{
			i--;
			continue;
		}

		// Bail (ctrl+c, ctrl+d)
		if (c == 3 || c == 4)
			bail("User interupt\n");

		// Backspace
		if (c == 8)
		{
			i -= 2;
			if (i < -1)
				i = -1;
			continue;
		}

		// Accept
		if (c == '\n' || c == '\r')
		{
			buffer[i] = '\0';
			printf("\n");
			return;
		}

		// Cancel (ESC)
		if (c == 27) 
		{
			i = -1;
			continue;
		}

		if (c >= 32 && c <= 126)
		{
			buffer[i] = c;
		}
		else // ignore
		{
			i--;
		}
	}
	printf("\n");
	return;
}

void copyToClipboard(uint8_t* text)
{
	size_t size = strlen(text) + 1;
	HGLOBAL hMem = GlobalAlloc(GMEM_MOVEABLE, size);
	memcpy(GlobalLock(hMem), text, size);
	GlobalUnlock(hMem);
	OpenClipboard(0);
	EmptyClipboard();
	SetClipboardData(CF_TEXT, hMem);
	CloseClipboard();
}

