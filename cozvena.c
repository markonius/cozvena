#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <ctype.h>

// gets_s is not defined by stdlib
#ifndef __STDC_LIB_EXT1__
char* gets_s(char* str, size_t n)
{
	if (n == 0 || n > SIZE_MAX || str == 0)
		return 0;

	size_t i;
	for (i = 0; i < n - 1; i++)
	{
		int c = getchar();

		if (c == '\n') {
			str[i] = '\0';
			return str;
		}

		if (c == EOF) {
			str[i] = '\0';
			return 0;
		}

		str[i] = (char)c;
	}

	// EOF or newline not reached in loop.
	// This is an error by the standard
	str[i] = '\0';
	return 0;
}
#endif

#include "libscrypt/libscrypt.h"
#include "libscrypt/b64.h"

#define MAX_STRING_LENGTH 256

void bail(uint8_t* errorMessage);
#ifdef _WIN32
#include "cozvena_windows.c"
#else
#include "cozvena_linux.c"
#endif

typedef struct {
	int32_t version;
	uint8_t acceptedCookies_string[6];
	uint8_t hideAccount_string[6];
	uint8_t clearSecondaryPassword_string[6];

	uint64_t master_N;
	uint32_t master_r;
	uint32_t master_p;
	size_t master_dkLen;
	uint8_t master_salt[MAX_STRING_LENGTH];
	uint8_t showHashcheck_string[6];
	bool showHashcheck;

	uint64_t secondary_N;
	uint32_t secondary_r;
	uint32_t secondary_p;
	size_t secondary_dkLen;
	uint8_t secondary_salt[MAX_STRING_LENGTH];
} Config;

static const char* configFormat =
	"{\n"
	"	\"version\": %d,\n"
	"	\"acceptedCookies\": %[truefals],\n"
	"	\"master\": {\n"
	"		\"N\": %d,\n"
	"		\"r\": %d,\n"
	"		\"p\": %d,\n"
	"		\"dkLen\": %d,\n"
	"		\"salt\": \"%[^\"\n]\",\n"
	"		\"showHashcheck\": %[truefals]\n"
	"	},\n"
	"	\"secondary\": {\n"
	"		\"N\": %d,\n"
	"		\"r\": %d,\n"
	"		\"p\": %d,\n"
	"		\"dkLen\": %d,\n"
	"		\"salt\": \"%[^\"\n]\",\n"
	"		\"clearSecondaryPassword\": %[truefals],\n"
	"		\"hideAccount\": %[truefals]\n"
	"	},\n"
	"	\"savedAccounts\": [\n"
	"%*[^]]"
	"]\n"
	"}\n"
	;

static const char* configFormatForPrint =
	"{\n"
	"	\"version\": %d,\n"
	"	\"acceptedCookies\": %s,\n"
	"	\"master\": {\n"
	"		\"N\": %d,\n"
	"		\"r\": %d,\n"
	"		\"p\": %d,\n"
	"		\"dkLen\": %d,\n"
	"		\"salt\": \"%s\",\n"
	"		\"showHashcheck\": %s\n"
	"	},\n"
	"	\"secondary\": {\n"
	"		\"N\": %d,\n"
	"		\"r\": %d,\n"
	"		\"p\": %d,\n"
	"		\"dkLen\": %d,\n"
	"		\"salt\": \"%s\",\n"
	"		\"clearSecondaryPassword\": %s,\n"
	"		\"hideAccount\": %s\n"
	"	},\n"
	"	\"savedAccounts\": [\n"
	"%s"
	"	]\n"
	"}\n"
	;

Config makeDefaultConfig()
{
	Config c;
	c.version = 6;
	strcpy(c.acceptedCookies_string, "true");
	strcpy(c.hideAccount_string, "false");
	strcpy(c.clearSecondaryPassword_string, "true");

	c.master_N = 32768;
	c.master_r = 8;
	c.master_p = 8;
	c.master_dkLen = 33;
	strcpy(c.master_salt, "stolna, morska");
	strcpy(c.showHashcheck_string, "true");
	c.showHashcheck = true;

	c.secondary_N = 2048;
	c.secondary_r = 8;
	c.secondary_p = 8;
	c.secondary_dkLen = 18;
	strcpy(c.secondary_salt, "stolna, morska");
	return c;
}

void printConfig(Config config)
{
	printf(configFormatForPrint,
			config.version,
			config.acceptedCookies_string,
			config.master_N,
			config.master_r,
			config.master_p,
			config.master_dkLen,
			config.master_salt,
			config.showHashcheck_string,
			config.secondary_N,
			config.secondary_r,
			config.secondary_p,
			config.secondary_dkLen,
			config.secondary_salt,
			config.clearSecondaryPassword_string,
			config.hideAccount_string,
			""
			);
}

Config tryLoadConfig(uint8_t* pathToConfigFile)
{
	Config config = makeDefaultConfig();

	printf("loading from %s\n", pathToConfigFile);

	FILE* file = fopen(pathToConfigFile, "r");
	if (file == NULL)
	{
		printf("Config file not found at %s. Loading default config.\n", pathToConfigFile);
		return config;
	}

	printf("loaded from %s:\n", pathToConfigFile);

	fseek(file, 0, SEEK_SET);
	char c;
	while (fread(&c, 1, 1, file))
	{
		printf("%c", c);
	}
	fseek(file, 0, SEEK_SET);

	int numCorrectReads = fscanf(file, configFormat,
			&config.version,
			config.acceptedCookies_string,
			&config.master_N,
			&config.master_r,
			&config.master_p,
			&config.master_dkLen,
			config.master_salt,
			config.showHashcheck_string,
			&config.secondary_N,
			&config.secondary_r,
			&config.secondary_p,
			&config.secondary_dkLen,
			config.secondary_salt,
			config.clearSecondaryPassword_string,
			config.hideAccount_string
			);

	fclose(file);

	printf("num correct reads: %d\n", numCorrectReads);

	// Parse bools
	{
		if (strcmp(config.showHashcheck_string, "true") == 0)
			config.showHashcheck = true;
		else
			config.showHashcheck = false;
		// Other bools are irrelevant to COzvena
	}

	return config;
}

void configureUI64(uint64_t* number, char* tooltip)
{
	printf("%s\n", tooltip);
	printf("> ");
	scanf("%"SCNu64, number);
}

void configureUI32(uint32_t* number, char* tooltip)
{
	printf("%s\n", tooltip);
	printf("> ");
	scanf("%"SCNu32, number);
}

void configureSize(size_t* number, char* tooltip)
{
	printf("%s\n", tooltip);
	printf("> ");
	scanf("%zd", number);
}

void configureString(uint8_t* string, char* tooltip)
{
	printf("%s\n", tooltip);
	printf("> ");
	gets_s(string, MAX_STRING_LENGTH);
}

void configureBool(uint8_t* string, bool* boolean, char* tooltip)
{
begin:
	printf("%s\n", tooltip);
	printf("[T]rue | [F]alse\n");
	printf("> ");
	char temp[6];
	gets_s(temp, 5);
	for (char* p = temp; *p; ++p) *p = tolower(*p);
	if (strcmp("true", temp) == 0 || strcmp("t", temp) == 0)
	{
		strcpy(string, "true");
		*boolean = true;
	}
	else if (strcmp("false", temp) == 0 || strcmp("f", temp) == 0)
	{
		strcpy(string, "false");
		*boolean = false;
	}
	else
	{
		fprintf(stderr, "Not a valid value.");
		goto begin;
	}
}

void configureMaster(Config* config)
{
	while (true)
	{
		printf("\nSelect an option:\n");
		printf(
			"[N] = %"PRIu64"\n"
			"[R] = %d\n"
			"[P] = %d\n"
			"[D]kLen = %zd\n"
			"[S]alt = %s\n"
			"Show [H]ashcheck = %s\n"
			"[B]ack\n"
			"> ",
			(*config).master_N,
			(*config).master_r,
			(*config).master_p,
			(*config).master_dkLen,
			(*config).master_salt,
			(*config).showHashcheck_string
		);

		char entry[MAX_STRING_LENGTH];
		gets_s(entry, MAX_STRING_LENGTH);
		for (char* p = entry; *p; ++p) *p = tolower(*p);

		if (strcmp("n", entry) == 0)
			configureUI64(&(*config).master_N,
					"General difficulty of the algorithm (linear scaling). Must be power of 2.");
		else if (strcmp("r", entry) == 0)
			configureUI32(&(*config).master_r,
					"Memory usage factor.");
		else if (strcmp("p", entry) == 0)
			configureUI32(&(*config).master_p,
					"Parallelization Deterent.");
		else if (strcmp("d", entry) == 0 || strcmp("dklen", entry) == 0)
			configureSize(&(*config).master_dkLen,
					"Output length in bytes. Multiply by 4/3 to get base64 output length. Should be multiple of 3.");
		else if (strcmp("s", entry) == 0 || strcmp("salt", entry) == 0)
			configureString((*config).master_salt,
					"Added to every hash to prevent rainbow attacks.");
		else if (strcmp("h", entry) == 0 || strcmp("show hashcheck", entry) == 0)
			configureBool((*config).showHashcheck_string, &(*config).showHashcheck,
					"Show a short hash of the master password that helps confirm it is correct.");
		else if (strcmp("b", entry) == 0 || strcmp("back", entry) == 0)
			return;
		else
			fprintf(stderr, "Invalid option\n");
	}
}

void configureSecondary(Config* config)
{
	while (true)
	{
		printf("\nSelect an option:\n");
		printf(
			"[N] = %"PRIu64"\n"
			"[R] = %d\n"
			"[P] = %d\n"
			"[D]kLen = %zd\n"
			"[S]alt = %s\n"
			"[B]ack\n"
			"> ",
			(*config).secondary_N,
			(*config).secondary_r,
			(*config).secondary_p,
			(*config).secondary_dkLen,
			(*config).secondary_salt
		);

		char entry[MAX_STRING_LENGTH];
		gets_s(entry, MAX_STRING_LENGTH);
		for (char* p = entry; *p; ++p) *p = tolower(*p);

		if (strcmp("n", entry) == 0)
			configureUI64(&(*config).secondary_N,
					"General difficulty of the algorithm (linear scaling). Must be power of 2.");
		else if (strcmp("r", entry) == 0)
			configureUI32(&(*config).secondary_r,
					"Memory usage factor.");
		else if (strcmp("p", entry) == 0)
			configureUI32(&(*config).secondary_p,
					"Parallelization Deterent.");
		else if (strcmp("d", entry) == 0 || strcmp("dklen", entry) == 0)
			configureSize(&(*config).secondary_dkLen,
					"Output length in bytes. Multiply by 4/3 to get base64 output length. Should be multiple of 3.");
		else if (strcmp("s", entry) == 0 || strcmp("salt", entry) == 0)
			configureString((*config).secondary_salt,
					"Added to every hash to prevent rainbow attacks.");
		else if (strcmp("b", entry) == 0 || strcmp("back", entry) == 0)
			return;
		else
			fprintf(stderr, "Invalid option\n");
	}
}

void writeConfig(Config config, uint8_t* pathToConfigFile)
{
	FILE* file = fopen(pathToConfigFile, "w");
	if (file == NULL)
	{
		fprintf(stderr, "Can't open %s for writing.\n", pathToConfigFile);
		return;
	}
	fprintf(file, configFormatForPrint,
			config.version,
			config.acceptedCookies_string,
			config.master_N,
			config.master_r,
			config.master_p,
			config.master_dkLen,
			config.master_salt,
			config.showHashcheck_string,
			config.secondary_N,
			config.secondary_r,
			config.secondary_p,
			config.secondary_dkLen,
			config.secondary_salt,
			config.clearSecondaryPassword_string,
			config.hideAccount_string,
			""
			);
	fclose(file);

	printf("Written to disk.\n");
}

void configure(Config* config, uint8_t* pathToConfigFile)
{
	printf("Current config:\n");
	printConfig(*config);
	while (true)
	{
		printf("\nSelect an option:\n");
		printf(
			"[M]aster\n"
			"[S]econdary\n"
			"[W]rite to disk\n"
			"[P]rint current config\n"
			"[Q]uit\n"
			"> "
		);

		char entry[MAX_STRING_LENGTH];
		gets_s(entry, MAX_STRING_LENGTH);
		for (char* p = entry; *p; ++p) *p = tolower(*p);

		if (strcmp("m", entry) == 0 || strcmp("master", entry) == 0)
			configureMaster(config);
		else if (strcmp("s", entry) == 0 || strcmp("secondary", entry) == 0)
			configureSecondary(config);
		else if (strcmp("w", entry) == 0 || strcmp("write", entry) == 0 || strcmp("write to disk", entry) == 0)
			writeConfig(*config, pathToConfigFile);
		else if (strcmp("p", entry) == 0 || strcmp("print", entry) == 0 || strcmp("print current config", entry) == 0)
			printConfig(*config);
		else if (strcmp("q", entry) == 0 || strcmp("quit", entry) == 0)
			exit(0);
		else
			fprintf(stderr, "Invalid option\n");
	}
}

void showHelp()
{
	printf("Usage: cozvena [pathToConfigFile | --config [pathToConfigFile] | --help]\n");
}

void bail(uint8_t* errorMessage)
{
	fprintf(stderr, "%s", errorMessage);
	showHelp();
	exit(1);
}

void cozvena(Config config)
{
	uint8_t rawOutput[MAX_STRING_LENGTH];

	uint8_t masterInput[MAX_STRING_LENGTH];
	uint8_t masterB64Output[MAX_STRING_LENGTH];

	uint8_t accountInput[MAX_STRING_LENGTH];
	uint8_t secondaryInput[MAX_STRING_LENGTH];
	uint8_t combinedInput[MAX_STRING_LENGTH];
	uint8_t secondaryB64Output[MAX_STRING_LENGTH];

	printf("Enter master password: ");
	getPass(masterInput, MAX_STRING_LENGTH);

	libscrypt_scrypt(masterInput, strlen(masterInput), config.master_salt, strlen(config.master_salt), config.master_N, config.master_r, config.master_p, rawOutput, config.master_dkLen);
	libscrypt_b64_encode(rawOutput, config.master_dkLen, masterB64Output, sizeof(masterB64Output));

	printf("\n");

	while(true)
	{
		printf("Enter account: ");
		gets_s(accountInput, MAX_STRING_LENGTH);
		printf("Enter secondary password: ");
		getPass(secondaryInput, MAX_STRING_LENGTH);

		strcpy(combinedInput, masterB64Output);
		strcat(combinedInput, accountInput);
		strcat(combinedInput, secondaryInput);

		libscrypt_scrypt(combinedInput, strlen(combinedInput), config.secondary_salt, strlen(config.secondary_salt), config.secondary_N, config.secondary_r, config.secondary_p, rawOutput, config.secondary_dkLen);
		libscrypt_b64_encode(rawOutput, config.secondary_dkLen, secondaryB64Output, sizeof(secondaryB64Output));

		copyToClipboard(secondaryB64Output);
		printf("Account password copied to clipboard.\n\n"); 
	}
}

int main(int argc, uint8_t* argv[])
{
	uint8_t* configFilePath = "./cozvenaConfig.crv";
	Config config;

	if (argc > 3)
		bail("Too many arguments\n");

	if (argc == 3)
	{
		if (strcmp("--config", argv[1]) != 0)
		{
			fprintf(stderr, "%s\n", argv[1]);
			bail("Unknown argument.\n");
		}

		configFilePath = argv[2];
		config = tryLoadConfig(configFilePath);
		configure(&config, configFilePath);
	}
	else if (argc == 2)
	{
		if (strcmp("--config", argv[1]) == 0)
		{
			config = tryLoadConfig(configFilePath);
			configure(&config, configFilePath);
		}
		else if (strcmp("--help", argv[1]) == 0)
		{
			showHelp();
		}
		else
		{
			if (strlen(argv[1]) >= 1)
			{
				if (argv[1][0] == '-')
				{
					fprintf(stderr, "%s\n", argv[1]);
					bail("Unknown argument.\n");
				}
			}

			configFilePath = argv[1];
			config = tryLoadConfig(configFilePath);
			cozvena(config);
		}
	}
	else if (argc == 1)
	{
		config = tryLoadConfig(configFilePath);
		cozvena(config);
	}
}
